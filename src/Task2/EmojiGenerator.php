<?php

declare(strict_types=1);

namespace App\Task2;

final class EmojiGenerator
{
    /**
     * @return \Generator
     */
    public function generate(): \Generator
    {
        yield from ['🚀', '🚃', '🚄', '🚅', '🚇'];
    }
}
