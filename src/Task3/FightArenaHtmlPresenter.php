<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $result = '';
        $fighters = $arena->all();
        if ($fighters !==[]) {
            foreach ( $fighters as $fighter) {
                $result .= '<p>' . '<img src="' . $fighter->getImage() . '">' . $fighter->getName() . ': ' . $fighter->getHealth(). ', ' . $fighter->getAttack(). '</p>';
            }
        } else {
            $result = 'Arena is empty, please add fighters';
        }
        return $result;
    }
}
