<?php

declare(strict_types=1);

namespace App\Task1;

final class FightArena
{
    /**
     * @var Fighter[]
     */
    private $fighters = [];

    /**
     * @param Fighter $fighter
     */
    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    /**
     * @return Fighter|null
     */
    public function mostPowerful(): ?Fighter
    {
        $fighters = $this->fighters;
        uasort($fighters, function (Fighter $a, Fighter $b) {
            return $a->getAttack() <=> $b->getAttack();
        });
        return array_pop($fighters);
    }

    /**
     * @return Fighter|null
     */
    public function mostHealthy(): ?Fighter
    {
        $result = null;
        foreach ($this->fighters as $fighter) {
            $result = $result ?? $fighter;
            assert($result instanceof Fighter);
            $result = $result->getHealth() < $fighter->getHealth() ? $fighter : $result;
        }
        return $result;
    }

    /**
     * @return Fighter[]
     */
    public function all(): array
    {
        return $this->fighters;
    }
}
